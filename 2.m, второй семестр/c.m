clc, clear, close all
A1 = rand(100);
[Q1, R1] = qr(A1);
D1 = linspace(10, 11, 100);
R1 = R1-diag(diag(R1))+diag(D1);
A1 = Q1*R1*Q1';
A1 = hess(A1);
B1 = rand(100);

A2 = rand(200);
[Q2, R2] = qr(A2);
D2 = linspace(50, 55, 200);
R2 = R2-diag(diag(R2))+diag(D2);
A2 = Q2*R2*Q2';
A2 = hess(A2);
B2 = rand(200);
figure 
spy(A1)
figure
spy(A2)

% d = eig(A)
tic
d11 = eig(A1);
t11 = toc;
d11 = sort(abs(d11));
err11 = norm(D1'-d11);
tic
d21 = eig(A2);
t21 = toc;
d21 = sort(abs(d21));
err21 = norm(D2'-d21);

% d = eig(A, B)
tic
d12 = eig(A1, B1);
t12 = toc;
tic
d22 = eig(A2, B2);
t22 = toc;

% [V, D] = eig(A)
tic
[V13, D13] = eig(A1);
t13 = toc;
err13 = norm(A1*V13-V13*D13);
tic
[V23, D23] = eig(A2);
t23 = toc;
err23 = norm(A2*V23-V23*D23);

% [V, D] = eig(A, 'nobalance')
tic
[V14, D14] = eig(A1, 'nobalance');
t14 = toc;
err14 = norm(A1*V14-V14*D14);
tic
[V24, D24] = eig(A2, 'nobalance');
t24 = toc;
err24 = norm(A2*V24-V24*D24);

% [V, D] = eig(A, B)
tic
[V15, D15] = eig(A1, B1);
t15 = toc;
err15 = norm(A1*V15-B1*V15*D15);
tic
[V25, D25] = eig(A2, B2);
t25 = toc;
err25 = norm(A2*V25-B2*V25*D25);

% [V, D] = eig(A, B, flag)
tic
[V16, D16] = eig(A1, B1, 'qz');
t16 = toc;
err16 = norm(A1*V16-B1*V16*D16);
tic
[V26, D26] = eig(A2, B2, 'qz');
t26 = toc;
err26 = norm(A2*V26-B2*V26*D26);

err11
t11
err21
t21
t12
t22
err13
t13
err23
t23
err14
t14
err24
t24
err15
t15
err25
t25
err16
t16
err26
t26

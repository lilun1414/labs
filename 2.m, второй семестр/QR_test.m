clc, clear, close all

A1 = rand(100);
[Q1, R1] = qr(A1);
D1 = linspace(1000, 10, 100);
R1 = R1-diag(diag(R1))+diag(D1);
A1 = Q1*R1*Q1';
A1 = hess(A1);

A2 = rand(500);
[Q2, R2] = qr(A2);
D2 = linspace(25000, 50, 500);
R2 = R2-diag(diag(R2))+diag(D2);
A2 = Q2*R2*Q2';
A2 = hess(A2);
A3 = A2;
A3(250, 100) = 1e-16;

A4 = rand(100);
[Q4, R4] = qr(A4);
D4 = linspace(11, 10, 100);
R4 = R4-diag(diag(R4))+diag(D4);
A4 = Q4*R4*Q4';
A4 = hess(A4);

A5 = rand(200);
[Q5, R5] = qr(A5);
D5 = linspace(55, 50, 200);
R5 = R5-diag(diag(R5))+diag(D5);
A5 = Q5*R5*Q5';
A5 = hess(A5);

A6 = rand(100);
[Q6, R6] = qr(A6);
D6 = [100 linspace(11, 10, 99)];
R6 = R6-diag(diag(R6))+diag(D6);
A6 = Q6*R6*Q6';
A6 = hess(A6);

A7 = rand(500);
[Q7, R7] = qr(A7);
D7 = [500 linspace(55, 50, 499)];
R7 = R7-diag(diag(R7))+diag(D7);
A7 = Q7*R7*Q7';
A7 = hess(A7);
A8 = A7;
A8(250, 100) = 1e-16;

A9 = rand(100);
[Q9, R9] = qr(A9);
D9 = [linspace(110, 100, 99) 10];
R9 = R9-diag(diag(R9))+diag(D9);
A9 = Q9*R9*Q9';
A9 = hess(A9);

A10 = rand(500);
[Q10, R10] = qr(A10);
D10 = [linspace(550, 500, 499) 50];
R10 = R10-diag(diag(R10))+diag(D10);
A10 = Q10*R10*Q10';
A10 = hess(A10);

A11 = rand(100);
[Q11, R11] = qr(A11);
D11 = [linspace(40, 1000, 94) 30+3i 30-3i 20+2i 20-2i 10+1i 10-1i];
R11 = R11-diag(diag(R11))+diag(D11);
A11 = Q11*R11*Q11';
A11 = hess(A11);

A12 = rand(500);
[Q12, R12] = qr(A12);
D12 = [linspace(25000, 200, 494) 150+15i 150-15i 100+10i 100-10i 50+5i 50-5i];
R12 = R12-diag(diag(R12))+diag(D12);
A12 = Q12*R12*Q12';
A12 = hess(A12);

A = {A1 A2 A3 A4 A5 A6 A7 A8 A9 A10 A11 A12};
D = {D1 D2 D2 D4 D5 D6 D7 D7 D9 D10 D11 D12};
N = 10^3;

Err = zeros(12, 1);
Time = zeros(12, 1);

for i = 1:12
    
    tic
    [d, err] = QR(A{i}, N);
    
    t = toc;
    
    Err(i) = err;
    Time(i) = t;
    
end
format short e
Time
Err

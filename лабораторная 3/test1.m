%%зависимость точности решения от числа обусловленности
clc, clear all, close all
delt = [];
C = [];
n = 10;
A = rand(n);
x  = ones(10,1);
for k = 1:5
        M = A;
        MD=M;
    for i=1:n
        MD(i,i)=MD(i,i)+i^(k)*sum(M(i,[1:i-1 i+1:n]));
    end
     C = [C cond(MD)];
     b = MD*x;
    [X] = yakobie(MD,b,1e-15);
    delt = [delt norm(X-x,"inf")];
end
figure 
semilogx(C,delt)
grid on

%%зависимость времени выполнения от числа обусловленности
clc, clear all, close all
C = [];
t = [];
n = 10;
A = rand(n);
x = ones(n,1);
for k = 1:6
     M = A;
     MD=M;
     for i=1:n
        MD(i,i)=MD(i,i)+i^(k)*sum(M(i,[1:i-1 i+1:n]));
     end
     C = [C cond(MD)];
     b = MD*x;
     tic
     [X] = yakobie(MD,b,1e-15);
     t = [t toc];
end
figure 
semilogx(C,t)
grid on

%%зависимость относительной погрешности от величин возмущения
clc, clear all, close all
n = 10;
B = rand(n)+10*eye(n);
x = ones(n,1);
[L,I] = min(B);
[N,J] = min(L);
delt1 = [];
delt2 = [];
for k = 0:3 
     A = B;
     b = A*x;
     for i = 1:length(b)
        b(i) = b(i)*zZalfa(k);
     end
     [X] = yakobie(A,b,1e-14);
     delt1 = [delt1 norm(X-x,"inf")/norm(x,"inf")]; 
     A(I(J),J) = A(I(J),J)*zZalfa(k);
     b = A*x;
     [X] = yakobie(A,b,1e-14);
     delt2 = [delt2 norm(X-x,"inf")/norm(x,"inf")]; 
end
figure
p = 0:3;
plot(p,delt1)
grid on
figure
plot(p,delt2)
grid on

function [dist]= zZalfa(alfa)
    if alfa  == 0
        dist = 1;
    else  
        dist = 1 + sign(randi([-1,1]))*(rand()/100+(alfa-1)/100);
        if dist == 1
            [dist] = zZalfa(alfa);
        end
    end
end



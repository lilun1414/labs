function [t, y] = ImpSys(A, a, b, y0, h)
t = a:h:b;
y = zeros(length(y0), length(t));
y(:, 1) = y0;

for i = 1:length(t)-1
y(:, i+1) = l_u(eye(size(A)) - h*A, y(:, i));
end
end
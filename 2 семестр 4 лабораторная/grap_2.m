%График фактической точности от заданной точности
clc, clear, close all

f = @(x, y) -y.^2.*(log(x) + 2).*log(x)./x + y./x;
ft = @(x) x./(x.*log(x).^2 + 1);
y0 = 1;
a = 1;
b = 2;
eps = 1e-7;

err1 = zeros(1, 5);
err2 = zeros(1, 5);
Err3 = zeros(1, 5);
Err4 = zeros(1, 5);
Err5 = zeros(1, 5);
Err6 = zeros(1, 5);
Err7 = zeros(1, 5);
A1 = zeros(1, 5);
A2 = zeros(1, 5);
A3 = zeros(1, 5);
A4 = zeros(1, 5);
A5 = zeros(1, 5);
A6 = zeros(1, 5);
A7 = zeros(1, 5);
Eps = zeros(1, 5);

for i = 1:5
    opts = odeset('RelTol', eps, 'AbsTol', eps);
    
        [t1, y1] = ode23(f, [a b], y0, opts);
        [t2, y2] = ode45(f, [a b], y0, opts);
        [t3, y3] = ode113(f, [a b], y0, opts);
        [t4, y4] = ode15s(f, [a b], y0, opts);
        [t5, y5] = ode23s(f, [a b], y0, opts);
        [t6, y6] = ode23t(f, [a b], y0, opts);
        [t7, y7] = ode23tb(f, [a b], y0, opts);

        err1(i) = max(abs(ft(t1)-y1));
        err2(i) = max(abs(ft(t2)-y2));
        Err3(i) = max(abs(ft(t3)-y3));
        Err4(i) = max(abs(ft(t4)-y4));
        Err5(i) = max(abs(ft(t5)-y5));
        Err6(i) = max(abs(ft(t6)-y6));
        Err7(i) = max(abs(ft(t7)-y7));
        A1(i) = length(t1);
        A2(i) = length(t2);
        A3(i) = length(t3);
        A4(i) = length(t4);
        A5(i) = length(t5);
        A6(i) = length(t6);
        A7(i) = length(t7);
    Eps(i) = eps;
    eps = eps*10;
    
end

figure
loglog(Eps, err1, 'o', Eps, err2, 'o', Eps, Err3, 'o', Eps, Err4, 'o', Eps, Err5, 'o', Eps, Err6, 'o', Eps, Err7, 'o')
xlabel('eps');
ylabel('err');
title('График фактической точности от заданной точности');
legend('ode23', 'ode45', 'ode113', 'ode15s', 'ode23s', 'ode23t', 'ode23tb', 'Location', 'NorthWest');

figure
loglog(Eps, A1, Eps, A2, Eps, A3, Eps, A4, Eps, A5, Eps, A6, Eps, A7)
xlabel('eps');
ylabel('err');
title('График числа итераций от заданной точности');
legend('ode23', 'ode45', 'ode113', 'ode15s', 'ode23s', 'ode23t', 'ode23tb');
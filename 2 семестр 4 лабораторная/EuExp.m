function [p, y] = EuExp(f, a, b, y0, h)
p = a:h:b;
y = zeros(length(y0), length(p));
y(:, 1) = y0;

for i = 1:length(p)-1
y(:, i+1) = y(:, i) + h*f(p(i), y(:, i));
end
end

%систеы 
clc, clear, close all
y0 = [1; 0; 1; 0; 1; 0; 1; 0; 1; 0];
S = linspace(1e2, 1e6, 20);
a = 0;
b = 1;
M1 = {};
M2 = {};

for i = 1:10
    l = 0.1;
    err1 = zeros(1, 20);
    err2 = zeros(1, 20);
    for j = 1:20
        D = linspace(-1, -S(i), 10);
        D = diag(D);
        V = rand(10);
        A = V*D*V^(-1);
        f = @(x, y) A*y;
        t = a:l:b;
        [X, Y] = ode15s(f, t, y0);
        [t1, y1] = ExpSys(A, a, b, y0, l);
        [t2, y2] = ImpSys(A, a, b, y0, l);
        err1(j) = norm(Y - y1');
        err2(j) = norm(Y - y2'); 
    end 
    M1{i} = err1;
    M2{i} = err2;
    l = l - 0.01;
end   

figure 
loglog(S, M2{1}, S, M2{2}, S, M2{3}, S, M2{4}, S, M2{5}, S, M2{6}, S, M2{7}, S, M2{8}, S, M2{9}, S, M2{10})
xlabel('s');
ylabel('err');
title('График зависимости погрешности от жёсткости для разного шага (неявный метод)');
legend('l = 0.1', 'l= 0.09', 'l= 0.08', 'l= 0.07', 'l= 0.06', 'l= 0.05', 'l= 0.04', 'l= 0.03', 'l= 0.02', 'l= 0.01', 'Location', 'NorthWest')

figure 
loglog(S, M1{1}, S, M1{2}, S, M1{3}, S, M1{4}, S, M1{5}, S, M1{6}, S, M1{7}, S, M1{8}, S, M1{9}, S, M1{10})
xlabel('s');
ylabel('err');
title('График зависимости погрешности от жёсткости для разного шага (явный метод)');
legend('l= 0.1', 'l= 0.09', 'l= 0.08', 'l= 0.07', 'l= 0.06', 'l= 0.05', 'l= 0.04', 'l= 0.03', 'l= 0.02', 'l= 0.01', 'Location', 'SouthEast')
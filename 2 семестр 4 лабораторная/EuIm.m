function [p, y] = EuIm(f, a, b, y0, h)
p = a:h:b;
y = zeros(length(y0), length(p));
y(:, 1) = y0;

for i = 1:length(p)-1
F = @(x) x - y(:, i) - h*f(p(i+1), x);
y(:, i+1) = bisection(F, a, b, 1e-15, 1e3);
end
end
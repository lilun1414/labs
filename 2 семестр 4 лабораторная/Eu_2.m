% Явный метод Эйлера
clc, clear, close all

f = @(x, y) 5*y - x.^2;
ft = @(x) x.^2 + 0.4.*x + 0.08;
y0 = 0.1;
a = 0;
b = 1;
l= 0.08;
X = {};
Y = {};

for i = 1:8
    [x, y] = EuExp(f, a, b, y0, l);
    X{i} = x;
    Y{i} = y;
    l= l- 0.01;
    b = b + 0.01;
end

figure 
plot(X{1}, Y{1}, X{2}, Y{2}, X{3}, Y{3}, X{4}, Y{4}, X{5}, Y{5}, X{6}, Y{6}, X{7}, Y{7}, X{8}, Y{8})
xlabel('x');
ylabel('y');
title('График решения для разного шага и отрезка интегрирования');
legend('l= 0.08, [a, b] = [0, 1]', 'l= 0.07, [a, b] = [0, 1.1]', 'l= 0.06, [a, b] = [0, 1.2]', 'l= 0.05, [a, b] = [0, 1.3]', 'l= 0.04, [a, b] = [0, 1.4]', 'l= 0.03, [a, b] = [0, 1.5]', 'l= 0.02, [a, b] = [0, 1.6]', 'Location', 'NorthWest')
grid minor

a1 = 0.08;
b1 = 0.5;
h1 = 0.08;
X1 = {};
Y1 = {};

for i = 1:8
    [x1, y1] = EuExp(f, a1, b1, y0, h1);
    X1{i} = x1;
    Y1{i} = y1;
    h1 = h1 - 0.01;
end

figure 
plot(X1{1}, Y1{1}, X1{2}, Y1{2}, X1{3}, Y1{3}, X1{4}, Y1{4}, X1{5}, Y1{5}, X1{6}, Y1{6}, X1{7}, Y1{7}, X1{8}, Y1{8})
xlabel('x');
ylabel('y');
title('График решения на отрезке [0.08, 0.5] для разного шага');
legend('l= 0.08', 'l= 0.07', 'l= 0.06', 'l= 0.05', 'l= 0.04', 'l= 0.03', 'l= 0.02', 'l= 0.01', 'Location', 'NorthWest')

b1 = 0.08;
b2 = 0.5;
HHH = {};
Errh = {};

for k = 1:3f
    Err = zeros(1, 7);
    H = zeros(1, 7);
    h2 = 0.08;
    for i = 1:7
        [x2, y2] = EuExp(f, b1, b2, y0, h2);
        err = max(abs(ft(x2) - y2));
        Err(i) = err;
        H(i) = h2;
        h2 = h2 - 0.01;
    end
    Errh{k} = Err;
    HHH{k} = H;
    b2 = b2 + 0.1;
end
% График изменения шага по отрезку
clc, clear, close all

f = @(x, y) -y.^2.*(log(x) + 2).*log(x)./x + y./x;
ft = @(x) x./(x.*log(x).^2 + 1);
y0 = 1;
a = 1;
b = 2;
N = 1e3;
eps = 1e-7;

opts = odeset('RelTol', eps, 'AbsTol', eps);
[t1, y1] = ode23(f, [a b], y0, opts);
[t2, y2] = ode45(f, [a b], y0, opts);
[t3, y3] = ode113(f, [a b], y0, opts);
[t4, y4] = ode15s(f, [a b], y0, opts);
[t5, y5] = ode23s(f, [a b], y0, opts);
[t6, y6] = ode23t(f, [a b], y0, opts);
[t7, y7] = ode23tb(f, [a b], y0, opts);

H1 = zeros(length(t1), 1);
for i = 1:length(t1)-1
    H1(i) = t1(i+1) - t1(i);
    end
H2 = zeros(length(t2), 1);

for i = 1:length(t2)-1
    H2(i) = t2(i+1) - t2(i);
    end
H3 = zeros(length(t3), 1);

for i = 1:length(t3)-1
    H3(i) = t3(i+1) - t3(i);
    end
H4 = zeros(length(t4), 1);

for i = 1:length(t4)-1
    H4(i) = t4(i+1) - t4(i);
    end
H5 = zeros(length(t5), 1);

for i = 1:length(t5)-1
    H5(i) = t5(i+1) - t5(i);
    end
H6 = zeros(length(t6), 1);

for i = 1:length(t6)-1
    H6(i) = t6(i+1) - t6(i);
    end
H7 = zeros(length(t7), 1);

for i = 1:length(t7)-1
    H7(i) = t7(i+1) - t7(i);
    end

figure
loglog(t1, H1, t2, H2, t3, H3, t4, H4, t5, H5, t6, H6, t7, H7)
xlabel('x');
ylabel('h');
title('График изменения шага по отрезку');
legend('ode23', 'ode45', 'od113', 'ode15s', 'ode23s', 'ode23t', 'ode23tb');

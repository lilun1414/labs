function c = bisection(f, a, b, eps, N)
  
    delta = 1e-17;
    fa = f(a);
    fb = f(b);
    
    for i = 1 : N

        c = 0.5*(a+b);
        tol = 0.5*(b-a);
        
        if tol < eps
            break
        end
        
        fc = f(c);
        
        if abs(fc) < delta
            break 
        end
        
        if fa * fc < 0
            b = c;
            fb = fc;
        else
            a = c;
            fa = fc;
        end
    end
end


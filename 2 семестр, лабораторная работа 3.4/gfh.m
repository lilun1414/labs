clc, clear, close all;

F1 = @(x) x.^4-3.2.*x.^3+2.5.*x.^2-7.*x-1.5;
F2 = @(x) x.^4-3.2.*abs(x-2.49).*x.^3+2.5.*x.^2-7.*x-1.5;
a = 0;
b = 5;
syms x
ite1 = int(F1, x, a, b);
ite2 = int(F2, x, a, b);
p = 10;
P = zeros(1, 8);
error1 = zeros(1, 8);
error2 = zeros(1, 8);

for i = 1:8
    
    I1 = integral(F1, a, b, 'AbsTol', 10^(-p));
    I2 = integral(F2, a, b, 'AbsTol', 10^(-p));
    error1 = abs(ite1 - I1);
    error2 = abs(ite2 - I2);
    error1(i) = error1;
    error2(i) = error2;
    
    P(i) = p;
    p = p - 1;
end

figure
plot(P, error1)
xlabel('p');
ylabel('Погрешность');
title('Зависимость погрешности от p');

figure
plot(P, error2)
xlabel('p');
ylabel('Погрешность');
title('Зависимость погрешности от p');



f = @(x) sin(x.^2).*(x.^4-3.2.*x.^3+2.5.*x.^2-7.*x-1.5);
a = 0;
b = 10;
syms x
It = int(f, x, a, b);
p = 10;
P = zeros(1, 8);
error1 = zeros(1, 8);
error2 = zeros(1, 8);

for i = 1:8
    
    I1 = integral(f, a, b, 'AbsTol', 10^(-p));
    I2 = quadgk(f, a, b);
    
    error1 = abs(It - I1);
    error2 = abs(It - I2);
    
    error1(i) = error1;
    error2(i) = error2;
    
    P(i) = p;
    p = p - 1;
end

figure
plot(P, error1)
xlabel('p');
ylabel('Погрешность');
title('Зависимость погрешности от p');

figure
plot(P, error2)
xlabel('p');
ylabel('Погрешность');
title('Зависимость погрешности от p');


F1 = @(x, y) x.^(y.^3)-3.2.*x.^3+2.5.*x.^2-7.*x-1.5;
F2 = @(x, y, z) x.^(y.^3)-3.2.*x.^3+2.5.*x.^2-7.*x-1.5*x.^z;
a = -1;
b = 1;
c = 0;
d = 1;
e = 0;
f = 1;
syms x
ite1 = integral2(F1, a, b, c, d);
ite2 = integral3(F2, a, b, c, d, e, f);
p = 10;
P = zeros(1, 8);
error1 = zeros(1, 8);
error2 = zeros(1, 8);

for i = 1:8
    
    I1 = integral2(F1, a, b, c, d, 'AbsTol', 10^(-p));
    I2 = integral3(F2, a, b, c, d, e, f, 'AbsTol', 10^(-p));
    
    error1 = abs(ite1 - I1);
    error2 = abs(ite2 - I2);
    
    error1(i) = error1;
    error2(i) = error2;
    
    P(i) = p;
    p = p - 1;
end

figure
plot(P, error1)
xlabel('p');
ylabel('Погрешность');
title('Зависимость погрешности от p');

figure
plot(P, error2)
xlabel('p');
ylabel('Погрешность');
title('Зависимость погрешности от p');


f = @(x,y) (x>=-2 & x<=10 & y>=0 & y<=1).*x.^(y.^3)-3.2.*x.^3+2.5.*x.^2-7.*x-1.5;
a = -1;
b = 1;
c = 0;
d = 1;
It = integral2(f, a, b, c, d, 'AbsTol', eps);
p = 10;
P = zeros(1, 8);
Err = zeros(1, 8);

for i = 1:8
    
    I = integral2(f, a, b, c, d, 'AbsTol', 10^(-p));
    
    err = abs(It - I);
    
    Err(i) = err;
    
    P(i) = p;
    p = p - 1;
end

figure
plot(P, Err)
xlabel('p');
ylabel('Погрешность');
title('Зависимость погрешности от p');

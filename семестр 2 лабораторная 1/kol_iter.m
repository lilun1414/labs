clc, clear all, close all
E = [];
Iter = [];
n = 12;

for i = 1:15
    eps = 10^(-i);
    E = [E eps];
    G = numgrid('A', n);
    A = delsq(G);
    k = size(A,1);
    P = ilup(A,1);
    x = ones(k,1);
    b = A*x;
    x0 = rand(k,1);
    [x1, err, niter, flag, a_err] = conjgrad(A,x0,b, P, 1000, eps);
    Iter = [Iter niter]; 
    if i == 15
        Err = a_err;
        p = 1:niter;
    end
end

figure
semilogx(E, Iter)
grid on
title('Зависимость кол-ва итераций от точности')
xlabel('точность')
ylabel('количество итераций')

figure
semilogy(p, Err)
grid on
title('Уменьшение ошибки с ходом итераций')
xlabel('количество итераций')
ylabel('ошибка')

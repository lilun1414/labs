function [x, err, niter, flag, a_err] = conjgrad(A,x,b,P,maxit, tol)
a_err = [];
flag = 0;
niter = 0;
bnrm2 = norm(b);

if (bnrm2 == 0)
    bnrm2 = 1;
end
r = b - A*x;
err = norm(r)/bnrm2;
if (err < tol)
    return 
end
for niter = 1:maxit
    y = Gauss(P,r,1);
    z = Gauss(P,y,2);
    rho = (r'*z);
    if niter > 1
        beta = rho/rho1;
        p = z + beta*p;
    else
        p = z;
    end
    q = A*p;
    alpha = rho/(p'*q);
    x = x + alpha*p;
    r = r - alpha*q;
    err = norm(r)/bnrm2;
    a_err = [a_err err];
    if (err <= tol)
        break 
    end
    rho1 = rho;
end
if (err > tol)
    flag = 1;
end
        
end

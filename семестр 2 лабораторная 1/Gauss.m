function [X] = Gauss(A,b,q)
n = size(A,1);
x = zeros(n,1);

if q == 1
 for i = 1:n
    S = 0;
    if i~=1
    for j = 1:i
         S = S + A(i,j)*x(j);
    end
    end
    x(i) = (b(i)-S);
end   
    
end
if q == 2
for i = n:-1:1
    S = 0;
    if i~=n
    for j = n:-1:i
         S = S + A(i,j)*x(j);
    end
    end
    x(i) = (b(i)-S)/A(i,i);
end
end
X = x;
end

clc, clear all, close all
delta = [];
time = [];
N = [];

for j = 12:2:64
G = numgrid('A', j);
A = delsq(G);
k = size(A,1);
N = [N k];
tic
P = ilup(A,1);
x = ones(k,1);
b = A*x;
x0 = rand(k,1);

x1 = conjgrad(A,x0,b, P, 1000, 1e-15);
time = [time toc];
delta = [delta norm(x1-x)];

end

figure 
semilogy(N,delta)
grid on
title('Зависимость точности от размера матрицы')
xlabel('размер матрицы', 'FontSize', 18)
ylabel('точность','FontSize', 18 )

figure 
plot(N,time)
grid on
title({'Зависимость времени выполнения'; 'от размера матрицы'})
xlabel('размер матрицы', 'FontSize', 18)
ylabel('время', 'FontSize', 18)

clc, clear all, close all
E = [];
Delta = [];
n = 12;

for i = 1:15
    eps = 10^(-i);
    E = [E eps];
    G = numgrid('A', n);
    A = delsq(G);
    k = size(A,1);
    P = ilup(A,1);
    x = ones(k,1);
    b = A*x;
    x0 = rand(k,1);
    x1 = conjgrad(A,x0,b, P, 1000, eps);
    Delta = [Delta norm(x1-x)/norm(x)]; 
end
x = 1e-15:1e-4:1e-1;
y = @(x) x;
figure
loglog(E, Delta)
hold on
plot(x, y(x))
grid on
title('Зависимость погрешности от точности')
xlabel('точность')
ylabel('погрешнотсь')

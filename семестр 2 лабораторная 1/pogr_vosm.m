clc, clear all, close all

Delta = [];
n = 12;

for j = 0:3
G = numgrid('A', n);
A = delsq(G);
k = size(A,1);
P = ilup(A,1);
x = ones(k,1);
b = A*x;
for i = 1:length(b)
    b(i) = b(i)*z(j);
end
x0 = rand(k,1);
x1 = conjgrad(A,x0,b, P, 1000, 1e-15);
Delta = [Delta norm(x1-x,"inf")/norm(x,"inf")]; 
end

figure
p = 0:3;
plot(p,Delta)
grid on
title({'График зависимости относительной погрешности';'от величины возмущения'})
xlabel('возмущение')
ylabel('погрешность')

function [dist]= z(alfa)
if alfa  == 0
    dist = 1;
else  
dist = 1 + sign(randi([-1,1]))*(rand()/100+(alfa-1)/100);
if dist == 1
    [dist] = z(alfa);
end
end
end


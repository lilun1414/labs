clc, clear, close all

f = @(x) x.^4-3.2.*x.^3+2.5.*x.^2-7.*x-1.5;
a = -2;
b = 2;
I = 302/15;
N = 10^3;
ep = 1e-10;  
[I1, k, Iarr, H] = TR(f, a, b, N, ep);
n = length(Iarr);
er = zeros(1, n);

for i = 1:n
    err = abs(I-Iarr(i));
    er(i) = err;
end
    
figure
loglog(H, er)
xlabel('длина отрезка');
ylabel('фактическая ошибка');
title('Зависимость фактической ошибки от длины отрезка разбиения');
grid minor

clc, clear, close all

f = @(x) x.^4-3.2.*x.^3+2.5.*x.^2-7.*x-1.5;
a = -2;
b = 2;
I = 302/15;
N = 10^3;
eps = 1e-10;

er = zeros(1, 8);
es = zeros(1, 8);

for k = 1:8
    
    I1 = TR(f, a, b, N, eps);
    
    err = abs(I-I1);
    er(k) = err;
    
    es(k) = eps;
    eps = eps*10;
    
end

fb = @(x) x;

figure
plot(log2(es), log2(er), 'o')
hold on
plot(log2(es), fb(log2(es)))
xlabel('фактическая ошибка');
ylabel('заданная точность');
title('Зависимость фактической ошибки от заданной точности');

grid minor
function [I1, k, Iarr, H] = TR(f, a, b, N, eps)
h = b-a;
n = 1;
I1 = h*(f(a)+f(b))/2;
H = [];
Iarr = [];

for k = 1:N
    I2 = I1;
    I1 = 0;
    for i = 1:n
        I1 = I1+f(a+(i-1)*h+h/2);
    end
    I1 = (I1*h+I2)/2;
    Iarr = [Iarr I1];
    H = [H h];
    tol = abs(I1-I2)/3;
    
    if tol < eps
        return 
    end
    
    n = n*2;
    h = h/2;
end
end


function [t, A] = Lobatto_C(n)

syms x
f = (x^2-1)^(n+1);
g = -((2^n)*factorial(n)*(x^2-1))^(-1);
S = diff(f, n);
P = sym2poly(S);
Pder = matlabFunction(diff(g*S, 1));

t = roots(P);
t = sort(t);
t = t(2:n+1);

A = zeros(n, 1);

for i = 1:n
    A(i) = 8*((n+1)/(n+2))*(((Pder(t(i))*(1-t(i)^2))^2)^(-1));
end

end

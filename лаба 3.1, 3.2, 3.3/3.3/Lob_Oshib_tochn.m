
clc, clear, close all
f = @(x) cos(2.*x).*(x.^4-3.2.*x.^3+2.5.*x.^2-7.*x-1.5);
a = 5;
b = 10;
N = 1e3;
eps = 1e-10;
syms x
It = int(f, x, a, b);
Err = zeros(1, 5);
Eps = zeros(1, 5);

for i = 1:5
    I = Lobatto(f, a, b, N, eps);
    Err(i) = abs(It - I);
    Eps(i) = eps;
    eps = eps*100;
end

fb = @(x) x;
figure
plot(log2(Eps), log2(Err), 'o')
hold on
plot(log2(Eps), fb(log2(Eps)))
xlabel('заданная точность');
ylabel('ошибка');
title('Зависимость фактической ошибки от заданной точности');
grid minor

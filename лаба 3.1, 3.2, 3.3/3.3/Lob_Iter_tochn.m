clc, clear, close all
f = @(x) cos(2.*x).*(x.^4-3.2.*x.^3+2.5.*x.^2-7.*x-1.5);
a = 5;
b = 10;
N = 1e3;
eps = 1e-10;
K = zeros(1, 5);
Eps = zeros(1, 5);
for i = 1:5
    [I, R, H, k, E, Iarr] = Lobatto(f, a, b, N, eps);
    K(i) = k;
    Eps(i) = eps;
    eps = eps*100;
end
plot(log2(Eps), log2(K))
xlabel('заданная точность');
ylabel('Количество итераций');
title('Зависимость количества итераций от заданной точности');
grid minor
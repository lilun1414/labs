function [I1, I2] = Lobatto_mem(f, a, b, t1, t2, A1, A2, K1, K2)

fa1 = (b - a)*f(a)/2;
fb1 = (b - a)*f(b)/2;

x1 = (a + b)/2 + (b - a)*t1/2;
x2 = (a + b)/2 + (b - a)*t2/2;

y1 = (b - a)*f(x1)/2;
y2 = (b - a)*f(x2)/2;

I1 = K1*(fa1 + fb1) + A1'*y1;
I2 = K2*(fa1 + fb1) + A2'*y2;

end

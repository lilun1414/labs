clc, clear, close all
f = @(x) cos(2.*x).*(x.^4-3.2.*x.^3+2.5.*x.^2-7.*x-1.5);
a = 5;
b = 10;
N = 1e3;
eps = 1e-10;
syms x
It = int(f, x, a, b);   
[I, R, H, k, E, Iarr] = Lobatto(f, a, b, N, eps);
n = length(Iarr);
Err = zeros(1, n);
for i = 1:n
    err = abs(It-Iarr(i));
    Err(i) = err;
end 
figure
loglog(H, Err)
xlabel('длина отрезка');
ylabel('ошибка');
title('Зависимость фактической ошибки от длины отрезка разбиения');
grid minor
clc, clear, close all
f = @(x) cos(2.*x).*(x.^4-3.2.*abs(x-7.49).*x.^3+2.5.*x.^2-7.*x-1.5);
a = 5;
b = 10;
N = 1e3;
eps = 1e-10;
syms x
It = double(int(f, x, a, b));
Err = zeros(1, 5);
Eps = zeros(1, 5);
for i = 1:5
    [I, R, H, k, E, Iarr] = Lobatto(f, a, b, N, eps);
    err = abs(It-I);
    Err(i) = err;
    Eps(i) = eps;
    eps = eps*100;  
end
fb = @(x) x;
figure
plot(log2(Eps), log2(Err), 'o')
hold on
plot(log2(Eps), fb(log2(Eps)))
xlabel('заданная точность');
ylabel('ошибка');
title('Зависимость фактической ошибки от заданной точности');
grid minor
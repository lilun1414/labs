clc, clear, close all

f = @(x) x.^4-3.2.*x.^3+2.5.*x.^2-7.*x-1.5;
a = -2;
b = 2;
N = 10^3;
eps = 1e-10;
K = zeros(1, 8);
es = zeros(1, 8);

for i = 1:8
    [I1, k] = TR(f, a, b, N, eps);
    K(i) = k;
    es(i) = eps;
    eps = eps*10;
end

plot(log2(es), log2(K))
xlabel('eps');
ylabel('K');
title('Зависимость числа итераций от заданной точности');
legend('TR');
grid minor
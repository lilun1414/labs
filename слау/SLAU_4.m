clc, close all, clear all
format long

%(lu метод)

t = [];
for n = 1:350
    A = rand(n); 
    X = ones(n, 1); 
    B = A*X; 
    tic 
    [L, U] = lu(A);
    Y = L\B;
    RES = U\Y;
    [t(n)] = toc; 
end

k=1:350;
figure
semilogy(k, t)
grid on

%(qr-метод)

t = [];
for n = 1:350
    A = rand(n); 
    X = ones(n, 1);
    B = A*X; 
    tic 
    [Q, R, p] = qr(A, 0); 
    res(p,:) = R\(Q\B); 
    [t(n)] = toc; 
end

figure
semilogy(k, t)
grid on

%(метод Холецкого)
t = [];
for n = 1:350
    A = rand(n);
    X = ones(n, 1);
    [u, d, v] = svd(A);
    d = eye(n); 
    M = u*d*v'; 
    M = M*M';
    B = M*X; 

    tic
    R = chol(M);
    res = R\(R'\B);
    [t(n)] = toc; 
end

figure
semilogy(k, t)
grid on

 a = [1 2 3];
 b = [4 5 6]; 
 
 n1 = norm(a);         
 n2 = norm(b,1);  
 n3 = norm(b);     
  
A = [1 2 3; 4 5 6; 7 8 9];
B = [1*10^8 2*10^8 3*10^8; 4*10^8 5*10^8 6*10^8; 7*10^8 8*10^8 9*10^8];
D_mA = det(A)
D_mB = det(B)
D_mA_2 = cond(A)
D_mB_2 = cond(B)

X = [1; 2; 2];
sol_A = A\X
sol_B = B\X

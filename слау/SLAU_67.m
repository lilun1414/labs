clc, close all, clear all
format long

A = [4 3 2; 7 5 -2; 0 3 2];
B = [2; 0; 5];
opts.UT = true;
Ex_Solve = linsolve(A, B);
Uggle = linsolve(A, B, opts); 
opts.UT = false;
opts.SYM = true;
SSS = linsolve(A, B, opts); 

A = rand(5);
tol = 1e-7;
maxit = 100;
B = rand(5,1);
X = pcg(A, B,tol,maxit);
n = norm(B-A*X)/norm(B);
cc = cond(A);
c = [];
iter = [];
for n = 1:10
    A = hilb(n);
    c(n) = cond(A);
    B = ones(n, 1);
    R = linsolve(A, B);
    tol = 1e-5;
    [X, flag,relres, iter(n)] = pcg(A, B, tol);
end
semilogx(c, iter, "o-")
grid on

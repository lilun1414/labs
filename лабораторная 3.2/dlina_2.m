clc, clear, close all
a3 = 1;
b3 = 1e7;    
eps = 1e-7;
H = [];
X = [];
for i = 1:3
    c = 4+4*(-i+(i+i)*rand())*0.01;
    f3 = @(x) 1/(x.^2+c.*x+3);
    [I3, s3, H3, X3] = ATR(f3, a3, b3, eps);
    figure
    semilogy(X3, H3)
    xlabel('коорината');
    ylabel('длина отрезка');
    title('Зависимость фактической длины отрезка разбиения от координаты');
    legend('f3');
    grid minor
end

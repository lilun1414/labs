clc, clear, close all
f1 = @(x) sin(x.^4).*(x.^4-3.2.*x.^3+2.5.*x.^2-7.*x-1.5);
f2 = @(x) ((3-4.*x).^(-1/5));
f3 = @(x) 1/(x.^2+4.*x+3);
a1 = 1;
b1 = 2;
a2 = 3/4+1e-9;
b2 = 1;
a3 = 1;
b3 = 1e7;

syms x
It1 = int(f1, x, a1, b1);
It2 = 5/16;
It3 = 0.5*log(2);
eps = 1e-7;
err1 = zeros(1, 5);
err2 = zeros(1, 5);
err3 = zeros(1, 5);
Eps = zeros(1, 5);
for k = 1:5
    I1 = ATR(f1, a1, b1, eps);
    I2 = ATR(f2, a2, b2, eps);
    I3 = ATR(f3, a3, b3, eps);
    er1 = abs(It1-I1);
    er2 = abs(It2-abs(I2));
    er3 = abs(It3-I3);
    err1(k) = er1;
    err2(k) = er2;
    err3(k) = er3;
    Eps(k) = eps;
    eps = eps*10;
end

fx = @(x) x;
figure
%plot(log2(Eps), log2(Err1), 'o', 'Color', 'red')
hold on
%plot(log2(Eps), log2(Err2), 'o', 'Color', 'green')
plot(log2(Eps), log2(err3), 'o', 'Color', 'blue')
plot(log2(Eps), fx(log2(Eps)))
xlabel('заданная точность');
ylabel('фактичекая ошибка');
title('Зависимость фактической ошибки от заданной точности');
legend('f1', 'f2', 'f3');
grid minor
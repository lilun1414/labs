function [m, fm, whole, h] = ATR_mem(f, a, fa, b, fb)
m = (a + b)/2;
fm = f(m);
h = b - a;
whole = h * (fa + fb) / 2;
end

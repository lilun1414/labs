clc, clear, close all
a3 = 1;
b3 = 1e7;
It3 = 0.5*log(2);
eps = 1e-7;
for i = 1:3
    c = 4+4*(-i+(i+i)*rand())*0.01;
    f3 = @(x) 1/(x.^2+c.*x+3);
    err = zeros(1, 5);
    Eps = zeros(1, 5);    
        for k = 1:5
            I3 = ATR(f3, a3, b3, eps);
            er3 = abs(It3-I3);
            err(k) = er3;
            Eps(k) = eps;
            eps = eps*10;
        end
        fx = @(x) x;
        figure
        plot(log2(Eps), log2(err), 'o')
        hold on
        plot(log2(Eps), fx(log2(Eps)))
        xlabel('заданная точность');
        ylabel('фактическая ошибка');
        title('Зависимость фактической ошибки от заданной точности');
        legend('f3');
        grid minor
end
function [I, i, H, X] = ATR_rec(f, a, fa, b, fb, eps, I, m, fm, i, H, X)
[lm, flm, Ia, hl] = ATR_mem(f, a, fa, m, fm);
[rm, frm, Ib, hr] = ATR_mem(f, m, fm, b, fb);
delta = Ia + Ib - I;
er = abs(delta)/3;  
i = i + 1;
if er <= eps
    I = Ia + Ib;
    H = [H hl hr];
    X = [X lm rm];
else
    [Ia, i1, H1, X1] = ATR_rec(f, a, fa, m, fm, eps/2, Ia, lm, flm, i, H, X);
    [Ib, i2, H2, X2] = ATR_rec(f, m, fm, b, fb, eps/2, Ib, rm, frm, i, H, X);
    I = Ia + Ib;
    i = i1 + i2;
    H = [H1 H2];
    X = [X1 X2];
end
end

clc, clear, close all
f1 = @(x) sin(x.^4).*(x.^4-3.2.*x.^3+2.5.*x.^2-7.*x-1.5);
f2 = @(x) ((3-4.*x).^(-1/5));
f3 = @(x) 1/(x.^2+4.*x+3);
a1 = 1;
b1 = 2;
a2 = 3/4+1e-9;
b2 = 1;
a3 = 1;
b3 = 1e7;  
eps = 1e-7; 
[I1, s1, H1, X1] = ATR(f1, a1, b1, eps);
[I2, s2, H2, X2] = ATR(f2, a2, b2, eps);
[I3, s3, H3, X3] = ATR(f3, a3, b3, eps);
figure
semilogy(X1, H1)
xlabel('координата');
ylabel('длина отрезка');
title('Зависимость фактической длины отрезка разбиения от координаты');
legend('f1');
grid minor
figure
semilogy(X2, H2)
xlabel('координата');
ylabel('длина отрезка');
title('Зависимость фактической длины отрезка разбиения от координаты');
legend('f2');
grid minor
figure
semilogy(X3, H3)
xlabel('координата');
ylabel('длина отрезка');
title('Зависимость фактической длины отрезка разбиения от координаты');
legend('f3');
grid minor

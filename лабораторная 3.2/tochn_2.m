clc, clear, close all
f1 = @(x) sin(x.^4).*(x.^4-3.2.*x.^3+2.5.*x.^2-7.*x-1.5);
f2 = @(x) ((3-4.*x).^(-1/5));
f3 = @(x) 1/(x.^2+4.*x+3);
a1 = 1;
b1 = 2;
a2 = 3/4+1e-9;
b2 = 1;
a3 = 1;
b3 = 1e7;  
eps = 1e-7;
S1 = zeros(1, 5);
S2 = zeros(1, 5);
S3 = zeros(1, 5);
Eps = zeros(1, 5);

for i = 1:5
    [I1, s1] = ATR(f1, a1, b1, eps);
    [I2, s2] = ATR(f2, a2, b2, eps);
    [I3, s3] = ATR(f3, a3, b3, eps);
    S1(i) = s1;
    S2(i) = s2;
    K3(i) = s3;
    Eps(i) = eps;
    eps = eps*10; 
end

figure
%plot(log2(Eps), log2(S1), 'Color', 'red')
hold on
%plot(log2(Eps), log2(S2), 'Color', 'green')
plot(log2(Eps), log2(K3), 'Color', 'blue')
xlabel('заданная точность');
ylabel('число итераций');
title('Зависимость числа итераций от заданной точности');
legend('f1', 'f2', 'f3');
grid minor

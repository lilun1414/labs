function x = Gauss_hess(A)

n = size(A, 1);
x = zeros(n, 1);

for i = 1:n-1
    t = A(i+1, i)/A(i, i);
    A(i+1, i+1:n) = A(i+1, i+1:n)-t*A(i, i+1:n);
end

x(n) = 1;

for j = n:-1:2
    s = A(j-1, n:-1:j)*x(n:-1:j);
    x(j-1) = -s/A(j-1, j-1);
end

end
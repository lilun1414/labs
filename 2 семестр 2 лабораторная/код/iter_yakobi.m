
n=30;
rep=1000;
TOL=[];
ERR1=[];
ERRV1=[];
ERR2=[];
ERRV2=[];
COUNT1=[];
COUNT2=[];
i=1;
% D = diag(1:n);
D=diag(0.1:0.1:0.1*n);
x0=ones(size(D, 1), 1);
A=rand(n);
[Q R]=qr(A);
A=Q'*D*Q;
[V1 d]=eig(A);
d=diag(d);
[d, in]=sort(d);
V=V1(:, in);
while i>(10^(-15))
    [l count]=cycjacobi(A, i);
    l=diag(l);
    l=sort(l);
    err=norm(d-l, inf);
    ERR1=[ERR1 err];
    X=eigvector(A, l);
    Err=[];
    for j = 1:n
        Err =[Err abs(sin(acos(X(:, j)'*V(:, j))/(norm(X(:, j))*norm(V(:, j)))))];
    end
    errv=max(Err);
    ERRV1 = [ERRV1 errv]; 
    COUNT1=[COUNT1 count];
    
    [L, count] = basicqr(A, rep, i);
    L=L(n:(-1):1);
    X=eigvector(A, L);
    [~, m]=max(d-L);
    err=norm(d-L, inf);
    ERR2=[ERR2 err];
    Err=[];
    for j = 1:n
        Err =[Err abs(sin(acos(X(:, j)'*V(:, j))/(norm(X(:, j))*norm(V(:, j)))))];
    end
    [errv num]=max(Err);
    ERRV2 = [ERRV2 errv];
    COUNT2=[COUNT2 count];
    TOL = [TOL i];
    i=i*0.1
end
loglog(TOL, ERR1);
hold on
loglog(TOL, ERR2);
grid on
xlabel('�������� ��������')
ylabel('�����������')
title('����������� ����������� �� �� �������� ��������(������ �����������)')
legend('����� �����')

loglog(TOL, ERRV1);
hold on
loglog(TOL, ERRV2);
grid on
xlabel('�������� ��������')
ylabel('�����������')
title('����������� ����������� �� �� �������� ��������(������ �����������)')
legend('����� �����')

semilogx(TOL, COUNT1)
grid on
xlabel('�������� ��������')
ylabel('����� ��������')
title('����������� ����� �������� �� �������� ��������(������ �����������)')

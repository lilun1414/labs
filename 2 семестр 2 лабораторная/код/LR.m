function [d, errd] = LR(A, N, eps)

n = size(A, 1);
d0 = ones(n, 1);

for i = 1:N
    
    [L, U] = LU(A);
    A = U*L;
    d = abs(diag(A));
    errd = norm(d-d0);
    
    if errd < eps
        break
    end
   
    d0 = d;
    
end

end
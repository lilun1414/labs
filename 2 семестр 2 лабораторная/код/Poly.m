function P = Poly(A)

n = size(A, 1);
T = zeros(n+1, 1);
P = zeros(1, n+1);
A1 = A;

P(1) = (-1)^n;
T(1) = sum(diag(A));
P(2) = -P(1)*T(1);

for i = 3:n+1
    A = A*A1;
    T(i-1) = sum(diag(A));
    P(i) = -(1/(i-1))*(P(i-1:-1:2)*T(1:i-2)+P(1)*T(i-1));
end

end
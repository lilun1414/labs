n=10;
tol = 10^(-13);
ERR1=[];
ERRV1=[];
ERR2=[];
ERRV2=[];
ERR3=[];
ERRV3=[];
D = diag(1:n);
%D=diag(0.1:0.1:0.1*n);
x0=ones(size(D, 1), 1);
A=rand(n);
[Q R]=qr(A);
A=Q'*D*Q;
[V1 d]=eig(A);
d=diag(d);
[d, I]=sort(d);
V=V1(:, I);
K=[];
for j=1:5
    for i=1:n
        A(n,i)=A(n, i)*(1+(-j+2*j*rand)/100);
    end
    [l count]=cycjacobi(A, tol);
    l=diag(l);
    l=sort(l)
    [~, m]=max(d-l);
    err=norm(d-l, inf)/norm(d(m));
    ERR1=[ERR1 err];
    Err=[];
    for i = 1:n
        Err =[Err abs(sin(acos(X(:, i)'*V(:, i))/(norm(X(:, i))*norm(V(:, i)))))];
    end
    [errv num]=max(Err);
    errv=errv/norm(V(:, num));
    ERRV1 = [ERRV1 errv]
    
    [L, count] = basicqr(A, rep, tol);
    L=L(n:(-1):1);
    X=eigvector(A, L);
    [~, m]=max(d-L);
    err=norm(d-L, inf)/norm(d(m));
    ERR2=[ERR2 err];
    Err=[];
    for i = 1:n
        Err =[Err abs(sin(acos(X(:, i)'*V(:, i))/(norm(X(:, i))*norm(V(:, i)))))];
    end
    [errv num]=max(Err);
    errv=errv/norm(V(:, num));
    ERRV2 = [ERRV2 errv];

    H=houshess(A);
    L=solver(H, tol);
    X=eigvector(A, L);
    [~, m]=max(d-L);
    err=norm(d-L, inf)/norm(d(m));
    ERR3=[ERR3 err];
    Err=[];
    for i = 1:n
        Err =[Err abs(sin(acos(X(:, i)'*V(:, i))/(norm(X(:, i))*norm(V(:, i)))))];
    end
    [errv num]=max(Err);
    errv=errv/norm(V(:, num));
    ERRV3 = [ERRV3 errv];
    K=[K j];
end
ERRV3
plot(K, ERR1)
hold on
plot(K, ERR1)
grid on
ylabel('Погрешность СЧ')
xlabel('Возмущение (в %)')
title('График зависимости погрешности СЧ от возмущения (метода Якоби)')
legend('Хорошая отделимость', 'Плохая отделимость');


plot(K, ERRV1)
hold on
plot(K, ERRV1)
grid on
ylabel('Погрешность СВ')
xlabel('Возмущение (в %)')
title('График зависимости погрешности СВ от возмущения (метод Якоби)')
legend('Хорошая отделимость', 'Плохая отделимость');



function A = Givens_hess(A)

n = size(A, 1);

for j = 1:n-2 
    for i = n:-1:j+2 
        
        B = A(j:n, j:n);
        G=eye(size(B, 1));
        r=sqrt(B(i-j, 1)^2+B(i-j+1, 1)^2);
        
        c = B(i-j, 1)/r;
        s = B(i-j+1, 1)/r;
        G(i-j, i-j) = c;
        G(i-j, i-j+1) = -s;
        G(i-j+1, i-j) = s;
        G(i-j+1, i-j+1) = c;
        
        G = blkdiag(eye(j-1), G);
       
        A = A*G;
        A = G'*A;
    end
end
end
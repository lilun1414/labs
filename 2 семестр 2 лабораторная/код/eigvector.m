function X=eigvector(A, l)
    X=zeros(size(A));
    n=size(A, 1);
    E=eye(size(A));
    P=A;
    b=l;
    for i = 1:n
        P=A-l(i)*E;
        [L U]=LUfunc(P);
        X(n, i)=1;
        b=-U(:, n);
        X(1:n-1, i)=LUsolve(E(1:n-1, 1:n-1), U(1:n-1, 1:n-1), b(1:n-1));
        X(:, i)=X(:, i)/norm(X(:, i));
    end
end

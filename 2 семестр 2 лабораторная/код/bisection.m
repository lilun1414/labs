function c = bisection(A, a, b, eps, N)

P = Poly(A);

fa = PolyVal(P, a);
fb = PolyVal(P, b);

for i = 1:N
    
    c = 0.5*(a+b);
    tol = 0.5*(b-a);
    
    if tol < eps
        break
    end
    
    fc = PolyVal(P, c);
    
    if fa * fc < 0
        b = c;
        fb = fc;
    else
        a = c;
        fa = fc;
    end
end

end




clc, clear, close all

N = 1e3;
delta = 1e-6;
eps = 1e-12;
y0 = ones(10, 1);

Mu = [];
Errd1 = [];
Errd2 = [];
Errx1 = [];
Errx2 = [];
Errd3 = [];
    
for n = 9:-0.5:1
    
    A = rand(10);
    [Q, R] = qr(A);
    D = linspace(10, n, 10);
    R = R-diag(diag(R))+diag(D);
    A = Q*R*Q';
    
    [V1, L1] = eig(A);
    m1 = max(L1(:));
    [r1, c1] = find(L1 == m1);
    
    Ah = hess(A);
    [Vh, Lh] = eig(Ah);
    L2 = sort(diag(Lh), 'descend');
    V2 = zeros(10);
    for i = 1:10
        [r2, c2] = find(Lh == L2(i));
        V2(:, i) = Vh(:, c2);
    end
    V2 = abs(V2);
    
    mu = D(1)/D(2);
    
    Mu = [Mu mu];
    
    [d1, x, errd1] = PM_norm(A, y0, N, delta, eps);
    errx1 = abs(sin(acos((V1(:, c1)'*x)/(norm(V1(:, c1))*norm(x)))));
    [d2, errd2] = LR(A, N, eps);
    [V, d3] = Eig(A, N, eps);
    errd3 = norm(D'-d3);
    V = abs(V);
    
    for i = 1:10
        errx = abs(sin(acos((V(:, i)'*V2(:, i))/(norm(V(:, i))*norm(V2(:, i))))));
        Err(i) = errx;
    end
    
    errx2 = max(Err);
    
    Errd1 = [Errd1 errd1];
    Errx1 = [Errx1 errx1];
    Errx2 = [Errx2 errx2];
    Errd2 = [Errd2 errd2];
    Errd3 = [Errd3 errd3];

end

figure
semilogy( Mu(1:5), Errd2(1:5));
xlabel('отделимость');
ylabel('погрешность определения СЧ');
title('График зависимости погрешности определения СЧ от отделимости');
legend('LR');
grid minor

figure
semilogy(Mu, Errd3, 'blue');
xlabel('отделимость');
ylabel('погрешность определения СВ');
title('График зависимости погрешности определения СЧ от отделимости');
legend('Хессенберг');
grid minor


figure
semilogy(Mu, Errx2);
xlabel('отделимость');
ylabel('погрешность определения СВ');
title('График зависимости погрешности определения СВ от отделимости');
legend('Хессенберг');
grid minor
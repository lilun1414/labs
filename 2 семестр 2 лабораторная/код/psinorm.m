function [psi] = psinorm(A)
n = max(size(A));
psi = 0;
for i=1:(n-1)
    for j=(i+1):n
        psi = psi + A(i,j)^2+A(j,i)^2;
    end
end
psi = sqrt(psi);
end
clc, clear, close all

N = 1e3;
delta = 1e-6;
eps = 1e-12;
y0 = ones(10, 1);

A = rand(10);
[Q, R] = qr(A);
D1 = linspace(10, 1, 10);
D2 = linspace(10, 9, 10);
R1 = R-diag(diag(R))+diag(D1);
R2 = R-diag(diag(R))+diag(D2);
A1 = Q*R1*Q';
B1 = A1;
A2 = Q*R2*Q';
B2 = A2;

[V11, L11] = eig(A1);
m11 = max(L11(:));
[r11, c11] = find(L11 == m11);

Ah1 = hess(A1);
[Vh1, Lh1] = eig(Ah1);
L21 = sort(diag(Lh1), 'descend');
V21 = zeros(10);
for i = 1:10
    [r21, c21] = find(Lh1 == L21(i));
    V21(:, i) = Vh1(:, c21);
end
V21 = abs(V21);

[V12, L12] = eig(A2);
m12 = max(L12(:));
[r12, c12] = find(L12 == m12);

Ah2 = hess(A2);
[Vh2, Lh2] = eig(Ah2);
L22 = sort(diag(Lh2), 'descend');
V22 = zeros(10);
for i = 1:10
    [r22, c22] = find(Lh2 == L22(i));
    V22(:, i) = Vh2(:, c22);
end
V22 = abs(V22);

Reld11 = zeros(1, 5);
Reld12 = zeros(1, 5);
Reld13 = zeros(1, 5);
Relx1 = zeros(1, 5);
Relv1 = zeros(1, 5);

Reld21 = zeros(1, 5);
Reld22 = zeros(1, 5);
Reld23 = zeros(1, 5);
Relx2 = zeros(1, 5);
Relv2 = zeros(1, 5);

Dist1 = zeros(1, 10);
Dist11 = zeros(1, 5);
Dist2 = zeros(1, 10);
Dist22 = zeros(1, 5);

for k = 1:5
    
    for i = 1:10
        B1(1, i) = A1(1, i)+A1(1, i)*(-k+(k+k)*rand())*0.01;
        dist1 = abs((B1(1, i)-A1(1, i))/A1(1, i));
        Dist1(i) = dist1;
    end
    
    dist11 = max(Dist1);
    Dist11(k) = dist11;
    
    for i = 1:10
        B2(1, i) = A2(1, i)+A2(1, i)*(-k+(k+k)*rand())*0.01;
        dist2 = abs((B2(1, i)-A2(1, i))/A2(1, i));
        Dist2(i) = dist2;
    end
    
    dist22 = max(Dist2);
    Dist22(k) = dist22;
    
    [d11, x1, errd11] = PM_norm(B1, y0, N, delta, eps);
    errx1 = abs(sin(acos((V11(:, c11)'*x1)/(norm(V11(:, c11))*norm(x1)))));
    [d12, errd12] = LR(B1, N, eps);
    [V13, d13] = Eig(B1, N, eps);
    V13 = abs(V13);
    Err1 = zeros(1, 10);
    for i = 1:10
        err1 = abs(sin(acos((V13(:, i)'*V21(:, i))/(norm(V13(:, i))*norm(V21(:, i))))));
        Err1(i) = err1;
    end
    
    errv1 = max(Err1);
    
    [d21, x2] = PM_norm(B2, y0, N, delta, eps);
    errx2 = abs(sin(acos((V12(:, c12)'*x2)/(norm(V12(:, c12))*norm(x2)))));
    d22 = LR(B2, N, eps);
    [V23, d23] = Eig(B2, N, eps);
    V23 = abs(V23);
    Err2 = zeros(1, 10);
    for i = 1:10
        err2 = abs(sin(acos((V23(:, i)'*V22(:, i))/(norm(V23(:, i))*norm(V22(:, i))))));
        Err2(i) = err2;
    end
    
    errv2 = max(Err2);
    
    reld11 = norm(d11-D1(1))/norm(D1(1));
    relx1 = errx1/norm(V11(:, c11));
    relv1 = errv1/norm(V21);
    reld12 = norm(d12-D1')/norm(D1);
    reld13 = norm(d13-D1')/norm(D1);
    
    reld21 = norm(d21-D2(1))/norm(D2(1));
    relx2 = errx2/norm(V12(:, c12));
    relv2 = errv2/norm(V22);
    reld22 = norm(d22-D2')/norm(D2);
    reld23 = norm(d23-D2')/norm(D2);
    
    Reld11(k) = reld11;
    Reld12(k) = reld12;
    Reld13(k) = reld13;
    Relx1(k) = relx1;
    Relv1(k) = relv1;
   
    Reld21(k) = reld21;
    Reld22(k) = reld22;
    Reld23(k) = reld23;
    Relx2(k) = relx2;
    Relv2(k) = relv2;
end




figure
plot(Dist11, Relv1, 'red', Dist22, Relv2, 'green');
xlabel('возмущение');
ylabel('погрешность');
title('График зависимости погрешности СВ от возмущения в матрице с хорошей и плохой отделимостью, Хессенберг');
legend('хорошая', 'плохая');
grid minor


figure
plot(Dist11, Reld12, 'red', Dist22, Reld22, 'green');
xlabel('возмущение');
ylabel('погрешность');
title('График зависимости погрешности от возмущения в матрице с хорошей и плохой отделимостью, LR');
legend('хорошая', 'плохая');
grid minor


figure
plot(Dist11, Reld13, 'red', Dist22, Reld23, 'green');
xlabel('возмущение');
ylabel('погрешность');
title('График зависимости погрешности от возмущения в матрице с хорошей и плохой отделимостью, Хессенберг');
legend('хорошая', 'плохая');
grid minor


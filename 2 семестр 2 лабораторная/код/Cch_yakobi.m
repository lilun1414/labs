n=10;
tol=10^(-13);
rep=1000;
SEP=[];
ERR1=[];
ERRV1=[];
ERR2=[];
ERRV2=[];
ERR3=[];
ERRV3=[];
for i=19:-0.5:1
    D = linspace(20, i, n);
    D=diag(D);
    x0=ones(size(D, 1), 1);
    A=rand(n);
    [Q R]=qr(A);
    A=Q'*D*Q;
    [V1 d]=eig(A)
    d=diag(d);
    [d, in]=sort(d);
    V=V1(:, in);
    sep=d(2)-d(1);
    SEP=[SEP sep];
    [l count]=cycjacobi(A, tol);
    l=diag(l);
    l=sort(l);
    err=norm(d-l, inf);
    ERR1=[ERR1 err];
    X=eigvector(A, l);
    Err=[];
    for i = 1:n
        Err =[Err abs(sin(acos(X(:, i)'*V(:, i))/(norm(X(:, i))*norm(V(:, i)))))];
    end
    errv=max(Err);
    ERRV1 = [ERRV1 errv];    
end
semilogy(SEP, ERR1);
xlabel('Отделимость');
ylabel('Погрешность определния СЧ');
title('График зависимости погрешности определения СЧ от отделимости');
legend('Метод Якоби');
grid minor
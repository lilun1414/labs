%f=@(x)3*sign(x)*x.^4+4*x.^3-12*x.^2+1;
f=@(x)tan(x);
a = 10;
SS=[];
A=[];
for j=0:5
    xx=[];
    yy=[];
    for i=1:a
        if (rand<0.5)
            d = -1;
        else
            d=1;
        end
        xx=[xx (0.5*(-1)*20*cos((2*i -1)*3.14/(2*a)))*(1+d*0.01*j*d+(rand*j*d*0.001))];
    end
    for i=1:length(xx)
        yy=[yy f(xx(i))];
    end
    x=[];
    y=[];
    for i=-5:0.1:5
        x=[x i];
    end
    for i=1:length(x)
        y=[y f(x(i))];
    end
     P=[];
    for i=1:length(x)
        P=[P New(xx, yy,x(i))];
    end
    PP=[];
    for i=1:length(x)
        if y(i)~=0
            PP(i)=abs((P(i)-y(i))/y(i));
        end
    end

    S= max(PP);
    SS=[SS S];
    A=[A j];
end
    plot(A,SS)
    grid on
    xlabel('процент возмущений исходных данных');
    ylabel('относительная погрешность')
    title('Зависимость относительной погрешности от процента возмущений исходных данных');
%f=@(x)3*sign(x)*x.^4+4*x.^3-12*x.^2+1;
f=@(x)tan(x);
xx=[];
yy=[];
x1 = -2;
x2 = 4;
a = 5

PP1=[];
    PP2=[];
    A=[];
     for a=10:100
        xx=[];
        yy=[];

        for i=-10:20/(a-1):10
            xx=[xx i];
        end
        for i=1:length(xx)
            yy=[yy f(xx(i))];
        end
        P1=Hermite(xx,yy,x1);
        P2=Hermite(xx,yy,x2);
        A1=f(x1);
        A2=f(x2);
        PP1=[PP1 abs(P1-A1)];
        PP2=[PP2 abs(P2-A2)];
        A=[A a];
     end
    semilogy(A,PP1)
    hold on
    semilogy(A,PP2)
    grid minor
    xlabel('количество узлов');
    ylabel('ошибка')
    title('Зависимость ошибки интерполяции от количества узлов в 2 точках');
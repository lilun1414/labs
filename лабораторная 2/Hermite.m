
function hh = Hermite(xX, yY, x0)
    L=length(xX); 
    
    hh = []; 
    S1 = [];
    S2 = [];
    S3 = [];
    S4 = [];
    d1 = (yY(1).*(2.*xX(1)-xX(3)-xX(2)))./((xX(1)-xX(2)).*(xX(1)-xX(3)))+...
          (yY(2).*(2.*xX(1)-xX(3)-xX(1)))./((xX(2)-xX(1)).*(xX(2)-xX(3)))+...
          (yY(3).*(2.*xX(1)-xX(2)-xX(1)))./((xX(3)-xX(1)).*(xX(3)-xX(2))) 

    d2 = (yY(length(xX) - 2).*(2.*xX(length(xX)) - xX(length(xX)  ) - xX(length(xX)-1)))./((xX(length(xX)-2) - xX(length(xX)-1)).*(xX(length(xX)-2) - xX(length(xX))))+...
          (yY(length(xX) - 1).*(2.*xX(length(xX)) - xX(length(xX)  ) - xX(length(xX)-2)))./((xX(length(xX)-1) - xX(length(xX)-2)).*(xX(length(xX)-1) - xX(length(xX))))+...
          (yY(length(xX)    ).*(2.*xX(length(xX)) - xX(length(xX)-1) - xX(length(xX)-2)))./((xX(length(xX)  ) - xX(length(xX)-2)).*(xX(length(xX)  ) - xX(length(xX)-1)))



    for k=1:L-1
        if k==1 
            dell2 = (yY(2)-yY(1))./(xX(2)-xX(1));
            dell3 = (yY(3)-yY(2))./(xX(3)-xX(2));
            dU1 = (((dell2).^(-1) +(dell3).^(-1))./2); 
            dF2 = 1/dU1;
            C=d1;
            D=yY(1);
            A=(dF2.*(xX(2)-xX(1)) - 2.*yY(2) + 2.*yY(1) + d1.*(xX(2)-xX(1)))./((xX(2)-xX(1)).^3);
            B=(3.*yY(2)- dF2.*(xX(2)-xX(1))-2.*d1.*(xX(2)-xX(1))- 3.*yY(1))./((xX(2)-xX(1)).^2);
            S1 = [A]; 
            S2 = [B];
            S3 = [C];
            S4 = [D];
            dF=dF2;
        elseif k~=L-1 && k~=1 
            C=dF;
            dell=(yY(k+2)-yY(k+1))./(xX(k+2)-xX(k+1)); 
            dell1=(yY(k+1)-yY(k))./(xX(k+1)-xX(k)); 

            if dell1*dell>0
                dU=(((dell).^(-1) +(dell1).^(-1))./2);
                dF=1/dU;
            else
                dF=0;
            end

            D = yY(k);
            A = (dF.*(xX(k+1)-xX(k)) - 2.*yY(k+1) + 2.*D + C.*(xX(k+1)-xX(k)))./((xX(k+1)-xX(k)).^3);
            B = (3.*yY(k+1)-dF.*(xX(k+1)-xX(k)) - 2.*C.*(xX(k+1)-xX(k)) - 3.*D)./((xX(k+1)-xX(k)).^2);

            S1 = [S1 A];
            S2 = [S2 B];
            S3 = [S3 C];
            S4 = [S4 D];

        elseif k==L-1 
            C=dF;
            dF=d2;
            D = yY(k);
            A = (dF.*(xX(k+1)-xX(k)) - 2.*yY(k+1) + 2.*D + C.*(xX(k+1)-xX(k)))./((xX(k+1)-xX(k)).^3);
            B = (3.*yY(k+1)-dF.*(xX(k+1)-xX(k)) - 2.*C.*(xX(k+1)-xX(k)) - 3.*D)./((xX(k+1)-xX(k)).^2);
            S1 = [S1 A];
            S2 = [S2 B];
            S3 = [S3 C];
            S4 = [S4 D];

        end
        
   
    hh = [ S1.' S2.' S3.' S4.'];
end

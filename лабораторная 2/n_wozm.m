%f=@(x)3*sign(x)*x.^4+4*x.^3-12*x.^2+1;
f=@(x)tan(x);
a = 10;
SS=[];
A=[];
for j=0:5
    xx=[];
    yy=[];
    for i=-2:20/(a-1):2
        if (rand<0.5)
            d = -1;
        else
            d=1;
        end
        xx=[xx i*(1+d*0.01*j*d+(rand*j*d*0.001))];
    end
    for i=1:length(xx)
        yy=[yy f(xx(i))];
    end
    x=[];
    y=[];
    for i=-2:0.1:2
        x=[x i];
    end
    for i=1:length(x)
        y=[y f(x(i))];
    end
     P=[];
    for i=1:length(x)
        P=[P New(xx, yy,x(i))];
    end
    PP=[];
    for i=1:length(x)
        if y(i)~=0
            PP(i)=abs((P(i)-y(i))/y(i));
        end
    end

    S= max(PP);
    SS=[SS S];
    A=[A j];
    end
   
plot(A,SS)
grid on
xlabel('процент возмущений исходных данных');
ylabel('относительная погрешность')
title('Зависимость относительной погрешности от процента возмущений исходных данных');
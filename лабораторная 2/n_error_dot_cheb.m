%f=@(x)3*sign(x)*x.^4+4*x.^3-12*x.^2+1;
f=@(x)tan(x);
a = 5;
xx=[];
    yy=[];

    for i=1:a
        xx=[xx (0.5*(-1)*20*cos((2*i -1)*3.14/(2*a)))];
    end
    for i=1:length(xx)
        yy=[yy f(xx(i))];
    end
    x=[];
    y=[];
    for i=-4:0.1:4
        x=[x i];
    end
    for i=1:length(x)
        y=[y f(x(i))];
    end
    P1=[];
    for i=1:length(x)
        P1=[P1 New(xx, yy,x(i))];
    end
    xxx=[];
    yyy=[];
    P2=[];
    for i=1:1.5*a
        xxx=[xxx (0.5*(-1)*20*cos((2*i -1)*3.14/(3*a)))];
    end
    for i=1:length(xxx)
        yyy=[yyy f(xxx(i))];
    end
    for i=1:length(x)
        P2=[P2 New(xxx, yyy,x(i))];
    end
    
    xxxx=[];
    yyyy=[];
    P3=[];
    for i=1:2*a
        xxxx=[xxxx (0.5*(-1)*20*cos((2*i -1)*3.14/(4*a)))];
    end
    for i=1:length(xxxx)
        yyyy=[yyyy f(xxxx(i))];
    end
    for i=1:length(x)
        P3=[P3 New(xxxx, yyyy,x(i))];
    end
    
    plot(x,y)
    hold on
    plot(x,P1)
    hold on
    plot(x,P2)
    hold on
    plot(x,P3)
    grid minor
    xlabel('x');
    ylabel('y')
    title('Иллюстрация работы полиномов');
    legend('функция','полином 5 степени','полином 8 степени','полином 10 степени');
    
    P0=[];
    for i=1:length(x)
        P0=[P0 NaN];
        P1(i)=abs(P1(i)-y(i));
        P2(i)=abs(P2(i)-y(i));
        P3(i)=abs(P3(i)-y(i));
    end
    figure
    plot(x,P0)
    hold on
    plot(x,P1)
    hold on
    plot(x,P2)
    hold on
    plot(x,P3)
    hold on
    grid minor
    xlabel('x');
    ylabel('y')
    title('График ошибки в каждой точке');
    legend('','полином 5 степени','полином 8 степени','полином 10 степени');
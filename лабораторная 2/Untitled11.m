x = linspace(-10, 10, 100);
f =@(x)3*x.^4+4*x.^3+12*x.^2+4;
y0 = 3*x.^4+4*x.^3+12*x.^2+4;
y = 3*x.^4+4*x.^3+12*x.^2+4 + 500*randn(size(x));

p = polyfit(x, y, 1);
xx = linspace(x(1), x(end), 10000);
yy1 = polyval(p, xx);

p = polyfit(x, y, 2);
yy2 = polyval(p, xx);

p = polyfit(x, y, 3);
yy3 = polyval(p, xx);

p = polyfit(x, y, 4);
yy4 = polyval(p, xx);

p = polyfit(x, y, 5);
yy5 = polyval(p, xx);

figure
subplot(6, 1, 1);
plot(x, y, '.', xx, yy1);
subplot(6, 1, 2);
plot(x, y, '.', xx, yy2);
subplot(6, 1, 3);
plot(x, y, '.', xx, yy3);
subplot(6, 1, 4);
plot(x, y, '.', xx, yy4);
subplot(6, 1, 5);
plot(x, y, '.', xx, yy5);
subplot(6, 1, 6);
plot(xx, f(xx)-yy4);

cftool
Y = y;
Y(1:100:10) = y(1:100:10) + 100;

x1 = linspace(-10, 10, 100);
y1 = (1+25*x.^2).^(-1) +randn(size(x1));

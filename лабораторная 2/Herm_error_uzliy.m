f=@(x)3*sign(x)*x.^4+4*x.^3-12*x.^2+1;
%f=@(x)tan(x);
xx=[];
yy=[];
a = 5

    SS=[];
    for a=10:100
        xx=[];
        yy=[];

        for i=-10:20/(a-1):10
            xx=[xx i];
        end
        for i=1:length(xx)
            yy=[yy f(xx(i))];
        end
        x=[];
        y=[];
        for i=-10:0.1:10
            x=[x i];
        end
        for i=1:length(x)
            y=[y f(x(i))];
        end
         P=[];
        for i=1:length(x)
            P=[P Hermite(xx, yy,x(i))];
        end
        for i=1:length(x)
            P(i)=abs(P(i)-y(i));
        end
        S= max(abs(P));
        SS=[SS S];
    end
    A=[];
    for a=10:100
        A=[A a];
    end
    semilogy(A,SS)
    grid on
    xlabel('количество узлов');
    ylabel('ошибка')
    title('Зависимость ошибки интерполяции от количества узлов');

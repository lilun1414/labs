function [yyy] = New(xX, yY, x0)
    n = length(xX);
    di = zeros(n);
    di(:, 1) = yY;
    pol = @(x)yY(n);
    neww = @(x)1;
    for i = 2 : n
        neww = @(x)neww (x).*(x - xX(n - i + 2));
        for j = 1 : n - i + 1
            di(j, i) = (di(j + 1, i - 1) - di(j, i - 1))/(xX(j + i - 1) - xX(j));
        end
        pol = @(x)pol(x) + di(n - i + 1, i)*neww(x);
    end
    yyy = pol(x0)
end
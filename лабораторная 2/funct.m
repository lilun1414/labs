clc,close all, clear all;
  x = linspace(-2,2);
 y = 3.*sign(x).*x.^4+4.*x.^3-12.*x.^2+1;
 figure
plot(x,y)
grid on

 x = linspace(-2,2);
 y = tan(x);
 figure
plot(x,y)
grid on
hold on 
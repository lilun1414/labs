clear all, clc, close all;
format long
f=@(x) sin(x) + x - 1;

%graph(0.2, f)         %%Строит график функции
%hord_2(f,0.2,1e-15,2)     %%Зависимость погрешности от числа вычислений функции sin(x) + x - 1(номера итерации).
hord_3(f,0.2,1e-15,2)      %%Зависимость использованного числа вычислений функции от заданной точности sin(x) + x - 1.
%hord_5(f,0.2,1e-15,2)      %%Зависимость относительной погрешности решения от возмущения исходных данных sin(x) + x - 1.
%fzeroFun

function [Xx,Yy] = graph(a, f)
    Xx = [];
    Yy=[];
    for i = -5:a:5
        Xx = [Xx i];
    end
    for j = 1:1:length(Xx)
        Yy(j) =  f(Xx(j));
    end
    plot(Xx,Yy)
end

function [] = hord_2(f, a, eps, xx)
    %%бисекция 2 Зависимость погрешности от числа вычислений функции (номера итерации)
   x0=fzero(f,xx)
    k = 20;
   Rf = [];
        [R] = hord(f,xx,a,eps)
      Kol = [];  

    for j=1:1:length(R)
        Rf(j) = abs(R(j)-x0);
        Kol = [Kol j];
    end
    plot(Kol,Rf)
end

function [Rf] = hord_3(f, a, eps, xx)
    %%бисекция 3 Зависимость погрешности от заданной точности.
    Rf = [];
    x0=fzero(f,xx);
    for j = 1:1:15
        [c] = hord(f,xx,a,eps)
        eps = eps*10;
        Rf(j) = c(length(c));
    end
    for j=1:1:15
        Rf(j) = abs(Rf(j)-x0)
    end
    
    for j = 1:1:15
        Kol(j) = 10.^(-(16-j));
    end
    
    
    z = 1e-15:0.000001:1;
    zz = @(z) z;
    loglog(Kol,Rf)
    hold on
    loglog(z,zz(z))
end

function [Rf] = hord_4( f, a, eps, xx)
    %/бисекция 4            Ось абсцисс взята в логарифмическом масштабе!
    %/ Зависимость использованного числа вычислений функции от заданной точности.
     Rf = [];
    x0=fzero(f,xx);

    for j = 1:1:15
         [c] = hord(f,xx,a,eps);
        eps = eps*10;
        R(j) = length(c);
    end
        
    for j = 1:15
        Kol(j) = j-16;
    end
    plot(Kol,R)
end


function [] = hord_5(f, a, eps, xx)
    %%бисекция 5 Зависимость относительной погрешности решения от возмущения исходных данных.
    d=0;
    Rh = [];
    x0=fzero(f,xx);
    for j=0:1:5
        if (rand<0.5)
            d = -1;
        else
            d=1;
        end 
        g = @(x) sin(x) + x - 1*(1+d*0.01*j+(rand*j*d*0.001));
        Ga = g(a);
        [R] = hord(g,xx,a,eps)
         Rh(j+1) = R(length(R));
    end
    
    for j=0:1:5
        Rh(j+1)=abs((Rh(j+1)-x0)/x0)
    end

    Kol = [0,1,2,3,4,5];
   
    plot(Kol,Rh)
end



function [x] = fzeroFun(g,x0,x)

    fun = @(x) sin(x) + x - 1;
    x0 = 2; 
    x = fzero(fun,x0)
end
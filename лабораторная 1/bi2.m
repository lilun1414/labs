clear all, clc, close all;
format long

f= @(x) x.^3-2*x.^2+7*x+3
%graph(0.2,f)        %%Строит график функции
%bisection_2(f,-3,0,1e-15,2)     %%Зависимость погрешности от числа вычислений функции (номера итерации).
%bisection_3(f,-3,0,1e-15,2)      %%Зависимость погрешности от заданной точности.
%bisection_4(f,-3,0,1e-15,2)      %%Зависимость использованного числа вычислений функции от заданной точности.
%bisection_5(f,-3,0,1e-15,2)      %%Зависимость относительной погрешности решения от возмущения исходных данных.
fzeroFun

function [Xx,Yy] = graph( a, f)
    Xx = [];
    Yy=[];
    for i = -5:a:5
        Xx = [Xx i];
    end
    for j = 1:1:length(Xx)
        Yy(j) =  f(Xx(j));
    end
    plot(Xx,Yy)
end

function [Rf] = bisection_2(f, a, b,eps, xx)
    %%бисекция 2 Зависимость погрешности от числа вычислений функции (номера итерации)
   x0=fzero(f,xx);
    k = 20;
   Rf = [];
        [c,i, Kol, R]=bisection(f,a,b,eps);
    
    for j=1:1:length(R)
        Rf(j) = abs(R(j)-x0);
    end
    
    plot(Kol,Rf)
end

function [Rf] = bisection_3(f, a, b, eps, xx)
    %%бисекция 3 Зависимость погрешности от заданной точности.
    Rf = [];
    x0=fzero(f,xx);
    for j = 1:1:15
        [c]=bisection(f,a,b,eps);
        eps = eps*10;
        Rf(j) = c;
    end
    for j=1:1:15
        Rf(j) = abs(Rf(j)-x0)
    end
    
    for j = 1:1:15
        Kol(j) = 10.^(-(16-j));
    end
    z = 1e-15:0.000001:1;
    zz = @(z) z;
    loglog(Kol,Rf)
    hold on
    loglog(z,zz(z))
end

function [Rf] = bisection_4( f, a, b, eps, xx)
    %/бисекция 4            Ось абсцисс взята в логарифмическом масштабе!
    %/ Зависимость использованного числа вычислений функции от заданной точности.
     Rf = [];
    x0=fzero(f,xx);

    for j = 1:1:15
         [c, i]=bisection(f,a,b,eps);
        eps = eps*10;
        R(j) = i
    end
        
    for j = 1:15
        Kol(j) = j-16;
    end
    plot(Kol,R)
end




function [] = bisection_5(f, a, b, eps, xx)
    %%бисекция 5 Зависимость относительной погрешности решения от возмущения исходных данных.
    d=0;
    Rh = [];
    x0=fzero(f,xx);
    for j=0:1:5
        if (rand<0.5)
            d = -1;
        else
            d=1;
        end 
        g = @(x) x.^3-2*x.^2+7*x+3*(1+d*0.01*j+(rand*j*d*0.001));
        Ga = g(a);
        [c] = bisection( g,a,b,eps)
         Rh(j+1) = c;
    end
    
    for j=0:1:5
        Rh(j+1)=abs((Rh(j+1)-x0)/x0)
    end

    Kol = [0,1,2,3,4,5];
   
    plot(Kol,Rh)
end



function [x] = fzeroFun(g,x0,x)

    fun = @(x) x.^3-2*x.^2+7*x+3
    x0 = 2; 
    x = fzero(fun,x0)
end


clear all, clc, close all;
format long

f=@(x) sqrt(x) - 1
%graph(0.2,f)        %%Строит график функции
%bisection_2(f,-3,0,1e-15,2)     %%Зависимость погрешности от числа вычислений функции (номера итерации).
%bisection_3(f,-3,0,1e-15,2)      %%Зависимость погрешности от заданной точности.
%bisection_4(f,-3,0,1e-15,2)      %%Зависимость использованного числа вычислений функции от заданной точности.
%bisection_5(f,-3,0,1e-15,2)      %%Зависимость относительной погрешности решения от возмущения исходных данных.
%hord_2(f,-1,1e-15,2)     %%Зависимость погрешности от числа вычислений функции 
%hord_3(f,-1,1e-15,2)      %%Зависимость погрешности от заданной точности
%hord_4(f,-1,1e-15,2)      %%Зависимость использованного числа вычислений функции от заданной точности
%fzeroFun


function [] = bisection_2(f, a, b,eps, xx)
    %Зависимость погрешности от числа вычислений 
   x0=fzero(f,xx);
    k = 20;
   Rf = [];
        [c,i, Kol, R]=bisection(f,a,b,eps);
    
    for j=1:1:length(R)
        Rf(j) = abs(R(j)-x0);
    end
    
    R(length(Rf))
    plot(Kol,Rf)
end

function [Rf] = bisection_3(f, a, b, eps, xx)
    %%бисекция 3 Зависимость погрешности от заданной точности.
    Rf = [];
    x0=fzero(f,xx);
    for j = 1:1:15
        [c]=bisection(f,a,b,eps);
        eps = eps*10;
        Rf(j) = c;
    end
    for j=1:1:15
        Rf(j) = abs(Rf(j)-x0)
    end
    
    for j = 1:1:15
        Kol(j) = 10.^(-(16-j));
    end
    z = 1e-15:0.000001:1;
    zz = @(z) z;
    loglog(Kol,Rf)
    hold on
    loglog(z,zz(z))
end

function [Rf] = bisection_4( f, a, b, eps, xx)
    %/бисекция 4            Ось абсцисс взята в логарифмическом масштабе!
    %/ Зависимость использованного числа вычислений функции от заданной точности.
     Rf = [];
    x0=fzero(f,xx);

    for j = 1:1:15
         [c, i]=bisection(f,a,b,eps);
        eps = eps*10;
        R(j) = i
    end
        
    for j = 1:15
        Kol(j) = j-16;
    end
    plot(Kol,R)
end




function [] = bisection_5(f, a, b, eps, xx)
    %%бисекция 5 Зависимость относительной погрешности решения от возмущения исходных данных.
    d=0;
    Rh = [];
    x0=fzero(f,xx);
    for j=0:1:5
        if (rand<0.5)
            d = -1;
        else
            d=1;
        end 
        g = @(x) sin(x) + x - 1*(1+d*0.01*j+(rand*j*d*0.001));
        Ga = g(a);
        [c] = bisection( g,a,b,eps)
         Rh(j+1) = c;
    end
    
    for j=0:1:5
        Rh(j+1)=abs((Rh(j+1)-x0)/x0)
    end

    Kol = [0,1,2,3,4,5];
   
    plot(Kol,Rh)
end



function [x] = fzeroFun(g,x0,x)

    fun = @(x) sin(x) + x - 1
    x0 = 2; 
    x = fzero(fun,x0)
end


function [] = hord_2(f, a, eps, xx)
    %%бисекция 2 Зависимость погрешности от числа вычислений функции (номера итерации)
   x0=fzero(f,xx)
    k = 20;
   Rf = [];
        [R] = hord(f,xx,a,eps)
      Kol = [];  

    for j=1:1:length(R)
        Rf(j) = abs(R(j)-x0);
        Kol = [Kol j];
    end
    plot(Kol,Rf)
end

function [Rf] = hord_3(f, a, eps, xx)
    %%бисекция 3 Зависимость погрешности от заданной точности.
    Rf = [];
    x0=fzero(f,xx);
    for j = 1:1:15
        [c] = hord(f,xx,a,eps)
        eps = eps*10;
        Rf(j) = c(length(c));
    end
    for j=1:1:15
        Rf(j) = abs(Rf(j)-x0)
    end
    
    for j = 1:1:15
        Kol(j) = 10.^(-(16-j));
    end
    
    
    z = 1e-15:0.000001:1;
    zz = @(z) z;
    loglog(Kol,Rf)
    hold on
    loglog(z,zz(z))
end

function [Rf] = hord_4( f, a, eps, xx)
    %/бисекция 4            Ось абсцисс взята в логарифмическом масштабе!
    %/ Зависимость использованного числа вычислений функции от заданной точности.
     Rf = [];
    x0=fzero(f,xx);

    for j = 1:1:15
         [c] = hord(f,xx,a,eps);
        eps = eps*10;
        R(j) = length(c);
    end
        
    for j = 1:15
        Kol(j) = j-16;
    end
    plot(Kol,R)
end


function [] = hord_5(f, a, eps, xx)
    %%бисекция 5 Зависимость относительной погрешности решения от возмущения исходных данных.
    d=0;
    Rh = [];
    x0=fzero(f,xx);
    for j=0:1:5
        if (rand<0.5)
            d = -1;
        else
            d=1;
        end 
        g = @(x) x.^3-2*x.^2+7*x+3*(d*0.01*j+(rand*j*d*0.001));
        Ga = g(a);
        [R] = hord(g,xx,a,eps)
         Rh(j+1) = R(length(R));
    end
    
    for j=0:1:5
        Rh(j+1)=abs((Rh(j+1)-x0)/x0)
    end

    Kol = [0,1,2,3,4,5];
   
    plot(Kol,Rh)
end

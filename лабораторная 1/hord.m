function [R] = hord(g,xX,a,eps)
    fa = g(a);
    R= [];
    for i = 1:1:200
       if (abs(g(xX))<eps)
           break;
       end
       xX = -fa*(xX-a)/(g(xX)-fa)+a;
       R(i) = xX;
    end
end

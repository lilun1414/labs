function [c, i , Kol, R] = bisection( f,a,b,eps)
     Fa = f(a);
     R = [];
     Kol = [];
     
    for i=1:100 
            c= (b+a)/2;
            y = f(c);
            if y*Fa<0 
                b=c;
            else
                a=c;
                Fa = y;
            end
        R(i) = c;
        Kol = [Kol i];
        if abs(b-a)<eps
            break
        end
    end
end

clc, close all, clear all
format long
len = 10;
X = ones(len, 1);
X0 = rand(len, 1)
eps = 10 ^ (-3);
n = 30;
A = rand(len);
A = A + A' + ones(len) * 10;
[u, d, v] = svd(A);
d = eye(len);
d(1,1) = 1;
M = u * d * v';
B = M * X; 

[L, U, P] = lu(M);
Y = L \ (P * B);
R0 = U \ Y;

for i = 1:3
    if rand < 0.5
        p = 1;
    else
        p = -1;
    end
    [aa, ind] = max(M);
    [bb, ind2] = max(aa);
    M(ind(ind2)) = M(ind(ind2)) * (1 + 0.01 * i * p);
    B = M * X;
    [R(:, i), ITER(i)] = Methsopr(M, B, X0, eps, n);
    ERR(i) = norm(R(i) - R0) / norm(R0);
    M(ind(ind2)) = bb;
    r(i) = i;
end

plot(r, ERR, "o-")
grid on

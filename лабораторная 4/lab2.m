clc, close all, clear all
format long
len = 10;
X = ones(len, 1); 
X0 = rand(len, 1);
eps = 10 ^ (-3);
n = 30;
A = rand(len);

A = A + A' + ones(len) * 10;
[u, d, v] = svd(A);
d = eye(len);
d(1,1) = 1;
M = u * d * v';
B = M * X; 
[L, U, P] = lu(M);
Y = L \ (P * B);
Resh = U \ Y;
for i = 1:3
    if rand < 0.5
        a = 1;
    else
        a = -1;
    end
    [A, ind] = max(B);
    B(ind) = B(ind) * (1 + 0.01 * i * a);
    [R(:, i), ITER(i)] = Methsopr(M, B, X0, eps, n);
    ERR(i) = norm(R(i) - Resh)/norm(Resh);
    r(i) = i;
    B(ind) = A;
end

plot(r, ERR, "o-")
grid on

clc, close all, clear all
%y = 3.*(x.^4)+4.*(x.^3)-12.*(x.^2)+1; 
a=-3; 
b=1.5; 
x=a:0.1:b; %45 точек
p=[3 4 -12 0 5]; %степени полинома
y=polyval(p,x) + 2*randn(size(x));
figure
plot(x,y,'b');
grid on
title("график исходного полинома 3x^4 + 4x^3 -12x^2+1")
legend("График полинома с шумом")

p1 = polyfit(x,y,1); %полиномиальное приближение для 1 степени
figure
t=linspace(a,b,1001); 
y1=polyval(p1,t);
subplot(2,1,1); 
plot(x,y,'b.',t,y1,'r')

legend("Данные 1", "Приближение 1 степени")
grid on

err1=y-polyval(p1,x); %ошибка для полиномиального приближения 1 степени
subplot(2,1,2); 
plot(x,err1,'r.')
grid on
legend("Ошибка 1")

p2=polyfit(x,y,2); %полиномиальное приближение для 2 степени
y2=polyval(p2,t);
figure
subplot(2,1,1); 
plot(x,y,'b.',t,y2,'g')
grid on
legend("Данные 2", "Приближение 2 степени")

err2=y-polyval(p2,x); %ошибка для полиномиального приближения 2 степени
subplot(2,1,2); 
plot(x,err2,'g.')
grid on
legend("Ошибка 2")

p3=polyfit(x,y,3); %полиномиальное приближение для 3 степени
y3=polyval(p3,t);
figure
subplot(2,1,1); 
plot(x,y,'b.',t,y3,'y')
grid on
legend("Данные 3", "Приближение 3 степени")

err3=y-polyval(p3,x); %ошибка для полиномиального приближения 3 степени
subplot(2,1,2); 
plot(x,err3,'y.')
grid on
legend("Ошибка 3")

p4=polyfit(x,y,4); %полиномиальное приближение для 4 степени
y4=polyval(p4,t);
figure
subplot(2,1,1); 
plot(x,y,'b.',t,y4,'k')
grid on
legend("Данные 4", "Приближение 4 степени")

err4=y-polyval(p4,x); %ошибка для полиномиального приближения 4 степени
subplot(2,1,2); 
plot(x,err4,'k.')
grid on
legend("Ошибка 4")

p5=polyfit(x,y,5); %полиномиальное приближение для 4+1=5 степени
y5=polyval(p5,t);
figure
subplot(2,1,1); 
plot(x,y,'b.',t,y5,'m')
grid on
legend("Данные 5", "Приближение 5 степени")

err5=y-polyval(p5,x); %ошибка для полиномиального приближения 5 степени
subplot(2,1,2); 
plot(x,err5,'m.')
legend("Ошибка 5")

grid on
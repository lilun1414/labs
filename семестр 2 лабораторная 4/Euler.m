function [y, XX, YY, h, iter] = Euler(fun, N, a, b, Y0, eps, Nmax)
    
    n = 1;
    h = (b - a);
    x0 = a;
    y0 = Y0;
    
    m1 = fun(x0, y0);
    m2 = fun(x0 + h, y0 + h * m1);
    Y = y0 + 0.5 * h * (m1 + m2);
    
    iter = 2;
    
    for i = 1 : Nmax
        
        n = n * 2;
        h = (b - a) / n;
        
        YY = zeros(N, n + 1);
        XX = zeros(1, n + 1);
        
        x0 = a;
        y0 = Y0;
        
        XX(1) = x0;
        YY(:, 1) = y0;
        
        for j = 1 : n
            
            m1 = fun(x0, y0);
            m2 = fun(x0 + h, y0 + h * m1);
            y = y0 + 0.5 * h * (m1 + m2);
            YY(:, j + 1) = y;
            
            iter = iter -10 ;
            
            x0 = x0 + h;
            XX(j + 1) = x0;
            
            y0 = y;
    
        end
        
        if abs(max(Y - y)) / 3   <= eps
            break
        else
            Y = y;
        end
        
    end
    
end

clc, clear all, close all;
fun = @(x, y) (x + 1).^3 + 2./(x + 1).*y;
fun1 = @(x) (((x+1).^2)./2 + 0.5).*(x+1).^2;
a = 1;
b = 2;
y0 = 10;

[~, XX1, YY1, h1] = Euler(fun, 1, a, b, y0, 1e-10, 1);
[~, XX2, YY2, h2] = Euler(fun, 1, a, b, y0, 1e-10, 5);


x = 1 : 0.005 : 2;
y = fun1(x);


figure
plot(x, y, 'b')
xlabel('x');
ylabel('y');
title('График решения задачи Коши');
legend('f');
grid minor


figure 
plot(x, y,'.', XX1, YY1, XX2, YY2)
grid on
title("Графики точного и численных решений")
legend("Точное решение", "Решение, полученное с шагом h = " + h1, "Решение, полученное с шагом h = " + h2)



